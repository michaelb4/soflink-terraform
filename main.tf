# DEFINE PROVIDER
resource "random_pet" "prefix" {}

provider "aws" {
  region     = "us-east-2"
  access_key = var.aws_accessKey
  secret_key = var.aws_secretKey
} 

# Create Security Group - Allow Port 22 Inbound
resource "aws_security_group" "ssh-secgrp" {
    name = "SofLink_EC2-SG-${random_pet.prefix.id}"
    ingress {
        protocol = "tcp"
        from_port = 22
        to_port = 22
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

# Create our EC2 Instances
resource "aws_instance" "ec2-ubuntu20_04" {
    ami = "ami-0fb653ca2d3203ac1"                           #ubuntu 20.04
    instance_type = var.ec2_instance_type 
    key_name = var.ec2_keypair                                
    associate_public_ip_address = true
    user_data = <<-EOF
        #!/bin/bash
        apt install -y git
        apt-get install -y software-properties-common
        # VULNERABLE DOCKER IMAGES
        apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
        curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
        add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian stretch stable"
        apt-get update
        apt-get install -y docker-ce
        systemctl start docker
        systemctl enable docker
        curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/sbin/docker-compose
        chmod +x /usr/local/sbin/docker-compose
        git clone https://github.com/csrez/SofLink.git /root/vulhub
        docker-compose -f "/root/vulhub/openssh/CVE-2018-15473/docker-compose.yml" up -d
        docker-compose -f "/root/vulhub/node/CVE-2017-16082/docker-compose.yml" up -d
        ### Grab rezilion instrument
        git clone https://"${var.gitlabUser}":"${var.gitlabPassword}"@gitlab.com/michaelb4/instrumentation.git
        cp /instrumentation/hermes /usr/local/sbin/
        chmod +x /usr/local/sbin/hermes
        # Write variables to service file
        sed -i "s/myapikey/${var.rezAPI}/" /instrumentation/hermes.service
        sed -i "s/yourenvname/${var.envName}/" /instrumentation/hermes.service
        cp /instrumentation/hermes.service /etc/systemd/system
        systemctl daemon-reload
        systemctl enable hermes.service
        systemctl start hermes.service
    EOF

    tags = {
        Name = "SofLink Ubuntu 20.04LTS-${random_pet.prefix.id}"
    }
    vpc_security_group_ids = [aws_security_group.ssh-secgrp.id]
}

resource "aws_instance" "ec2-amazonLinux" {
    ami = "ami-002068ed284fb165b"                           #Amazon-Linux1
    instance_type = var.ec2_instance_type 
    key_name = var.ec2_keypair                                 
    associate_public_ip_address = true

    user_data = <<-EOF
        #!/bin/bash
        yum install -y mariadb git wget
        amazon-linux-extras install -y nginx1
        yum install -y python3 docker log4j
        yum install -y git-core zlib zlib-devel gcc-c++ patch readline readline-devel libyaml-devel libffi-devel openssl-devel make bzip2 autoconf automake libtool bison curl sqlite-devel
        yum install -y ruby
        yum install -y nodejs 
        gems install -y rails
        systemctl start docker
        systemctl enable docker
        systemctl enable httpd
        systemctl start nginx
        systemctl enable nginx
        systemctl start httpd
        # VULNERABLE DOCKER IMAGES
        curl -s -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/sbin/docker-compose
        chmod +x /usr/local/sbin/docker-compose
        git clone https://github.com/csrez/SofLink.git /root/vulhub
        docker-compose -f "/root/vulhub/log4j/CVE-2021-44228/docker-compose.yml" up -d
        docker-compose -f "/root/vulhub/log4j/CVE-2017-5645/docker-compose.yml" up -d
                ### Grab rezilion instrument
        git clone https://"${var.gitlabUser}":"${var.gitlabPassword}"@gitlab.com/michaelb4/instrumentation.git
        cp /instrumentation/hermes /usr/local/sbin/
        chmod +x /usr/local/sbin/hermes
        # Write variables to service file
        sed -i "s/myapikey/${var.rezAPI}/" /instrumentation/hermes.service
        sed -i "s/yourenvname/${var.envName}/" /instrumentation/hermes.service
        cp /instrumentation/hermes.service /etc/systemd/system
        systemctl daemon-reload
        systemctl enable hermes.service
        systemctl start hermes.service
    EOF

    tags = {
        Name = "SofLink Amazon Linux 1-${random_pet.prefix.id}"
    }
    vpc_security_group_ids = [aws_security_group.ssh-secgrp.id]
}

resource "aws_instance" "ec2-debian9" {
    ami = "ami-08d3197b48e755ddb"                           #Debian9 Stretch
    instance_type = var.ec2_instance_type 
    key_name = var.ec2_keypair                                 
    associate_public_ip_address = true
    user_data = <<-EOF
        #!/bin/bash
        apt install -y git
        apt-get install -y software-properties-common
        # VULNERABLE DOCKER IMAGES
        apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
        curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
        add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian stretch stable"
        apt-get update
        apt-get install -y docker-ce
        systemctl start docker
        systemctl enable docker
        curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/sbin/docker-compose
        chmod +x /usr/local/sbin/docker-compose
        git clone https://github.com/csrez/SofLink.git /root/vulhub
        docker-compose -f "/root/vulhub/saltstack/CVE-2020-11651/docker-compose.yml" up -d
                ### Grab rezilion instrument
        git clone https://"${var.gitlabUser}":"${var.gitlabPassword}"@gitlab.com/michaelb4/instrumentation.git
        cp /instrumentation/hermes /usr/local/sbin/
        chmod +x /usr/local/sbin/hermes
        # Write variables to service file
        sed -i "s/myapikey/${var.rezAPI}/" /instrumentation/hermes.service
        sed -i "s/yourenvname/${var.envName}/" /instrumentation/hermes.service
        cp /instrumentation/hermes.service /etc/systemd/system
        systemctl daemon-reload
        systemctl enable hermes.service
        systemctl start hermes.service
    EOF

    tags = {
        Name = "SofLink Debian 9-${random_pet.prefix.id}"
    }
    vpc_security_group_ids = [aws_security_group.ssh-secgrp.id]
}

resource "aws_instance" "ec2-centOS7" {
    ami = "ami-02cae3195fa1622a8"                           #CentOS 7
    instance_type = var.ec2_instance_type 
    key_name = var.ec2_keypair                                 
    associate_public_ip_address = true
    user_data = <<-EOF
        #!/bin/bash
        yum install -y httpd git
        yum install -y python3 docker
        yum install -y git-core zlib zlib-devel gcc-c++ patch readline readline-devel libyaml-devel libffi-devel openssl-devel make bzip2 autoconf automake libtool bison curl sqlite-devel
        yum install -y ruby
        curl -sL https://rpm.nodesource.com/setup_16.x > /usr/local/sbin/my_node.sh
        chmod +x /usr/local/sbin/my_node.sh
        my_node.sh
        yum install -y nodejs 
        systemctl start docker
        systemctl enable docker
        systemctl enable httpd
        systemctl start httpd
        curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/sbin/docker-compose
        chmod +x /usr/local/sbin/docker-compose
        git clone https://github.com/csrez/SofLink.git /root/vulhub
        docker-compose -f "/root/vulhub/spring/CVE-2018-1270/docker-compose.yml" up -d
        docker run -dp 5000:5000 varsubham/sample_node
                ### Grab rezilion instrument
        git clone https://"${var.gitlabUser}":"${var.gitlabPassword}"@gitlab.com/michaelb4/instrumentation.git
        cp /instrumentation/hermes /usr/local/sbin/
        chmod +x /usr/local/sbin/hermes
        # Write variables to service file
        sed -i "s/myapikey/${var.rezAPI}/" /instrumentation/hermes.service
        sed -i "s/yourenvname/${var.envName}/" /instrumentation/hermes.service
        cp /instrumentation/hermes.service /etc/systemd/system
        systemctl daemon-reload
        systemctl enable hermes.service
        systemctl start hermes.service
    EOF

    tags = {
        Name = "SofLink CentOS 7-${random_pet.prefix.id}"
    }
    vpc_security_group_ids = [aws_security_group.ssh-secgrp.id]
}

resource "aws_instance" "ec2-redhat8" {
    ami = "ami-0ba62214afa52bec7"                           #RHEL 8
    instance_type = var.ec2_instance_type 
    key_name = var.ec2_keypair                                 
    associate_public_ip_address = true
    user_data = <<-EOF
        #!/bin/bash
        dnf install -y git
        # VULNERABLE DOCKER IMAGES
        dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
        dnf install docker-ce --nobest -y
        systemctl start docker
        systemctl enable docker
        curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/sbin/docker-compose
        chmod +x /usr/local/sbin/docker-compose
        git clone https://github.com/csrez/SofLink.git /root/vulhub
        docker-compose -f "/root/vulhub/weblogic/CVE-2017-10271/docker-compose.yml" up -d
        docker-compose -f "/root/vulhub/tomcat/CVE-2017-12615/docker-compose.yml" up -d
                ### Grab rezilion instrument
        git clone https://"${var.gitlabUser}":"${var.gitlabPassword}"@gitlab.com/michaelb4/instrumentation.git
        cp /instrumentation/hermes /usr/local/sbin/
        chmod +x /usr/local/sbin/hermes
        # Write variables to service file
        sed -i "s/myapikey/${var.rezAPI}/" /instrumentation/hermes.service
        sed -i "s/yourenvname/${var.envName}/" /instrumentation/hermes.service
        cp /instrumentation/hermes.service /etc/systemd/system
        systemctl daemon-reload
        systemctl enable hermes.service
        systemctl start hermes.service
    EOF
    tags = {
        Name = "SofLink RedHat EL8-${random_pet.prefix.id}"
    }
    vpc_security_group_ids = [aws_security_group.ssh-secgrp.id]
}

output "Ubuntu_IP" {value = aws_instance.ec2-ubuntu20_04.public_ip}
output "AmazonLinux_IP" {value = aws_instance.ec2-amazonLinux.public_ip}
output "Debian9_IP" {value = aws_instance.ec2-debian9.public_ip}
output "CentOS7_IP" {value = aws_instance.ec2-centOS7.public_ip}
output "RedHat8_IP" {value = aws_instance.ec2-redhat8.public_ip}
