variable "ec2_keypair" {}
variable "ec2_instance_type" {}
variable "aws_accessKey" {}                 # AWS access key (IAM)
variable "aws_secretKey" {}                 # AWS secret key (IAM)
variable "gitlabUser" {}                    # Deployment key username
variable "gitlabPassword" {}                # Deployment key password
variable "rezAPI" {}                        # Rezilion Olympus API Key found in Dashboard
variable "envName" {}                       # Environment name for the hermes command; shows up as "site" in dashboard
