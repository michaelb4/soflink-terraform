ec2_instance_type           = "t2.small"
gitlabUser                  = "gitlab+deploy-token-853764"

# MUST SET ALL THESE
ec2_keypair                 = "CS_BlueKey_East2"
rezAPI                      = "<rezilion-api-key-here>"                           # Set this; found in dahsboard - csv download
envName                     = "<your_env_name_here>"                                  # Environment name for the hermes command; shows up as "site" in dashboard


# For dev & troubleshooting; can hard-code these so you don't have to keep entering them on terraform apply
# !!! Do not upload these to the cloud or any git/storage accounts !!!
#aws_accessKey                   = "<your_aws_access_key_here>"
#aws_secretKey                   = "<your_aws_secret_key_DO_NOT_UPLOAD>"
#gitlabPassword                  = "<gitlab_password_here>"
