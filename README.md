# terraform-SofLink Demo Environment
 Create AWS EC2 environment to simulate client env for Demo Purposes.
 
PreReqs:
- Terraform
- Access to AWS environment
- Deployment token to gitlab environment (ask Blue)

Optional:
- git (if you want to use git from the command line to download these files)

AWS Setup: 
 1. You will need to create (or utilize) your Access Key ID and Secret Key from your AWS user
-- Go to IAM, select your user
-- Go to "Security Credentials" and "Create Access Key"
   -- You will need both the access key and secret key.  Save these in a secure location (1password)! Need these to run the script!
 
 2. You will need an SSH key created in the region that matches your configuration (East-2).
 -- Navigate to "EC2" and switch to the region (East-2)
 -- On the left navigation pane, go to "Key Pairs"
 -- Click "Create Key Pair" (only need to do this once)
 -- Create key pair. Use a name that is identifiable to your project. Remember this name, it will go in your tfvars file.
    -- Select RSA, .pem
    -- Click "Create key pair"
    -- Download the *.pem file to a secure location, as this is the private key that will be used to SSH into EC2 instances.


 Terraform Instructions:

 1. Install Terraform on your pc: https://www.terraform.io/downloads

 2. Copy the files in this git (SofLink Terraform) to a clean directory on your machine (new dir with no files)

 2.1 Set variables in my.tfvars
 - Set your rezAPI variable for the API for the Rezilion dashboard / backend
 - Set your Environment name; this becomes the "Site name" in the dashboard
 - Add your ssh key name exactly as you created it in AWS. If you forgot the name, you can look it up in "EC2" -> "Key Pairs"

 2.2 Save my.tfvars file
 
 3. Open a terminal window and change into the directory you copied the files into

 4. Run:  `terraform init`
 -- This will download the AWS provider from hashicorp

 5. Run:  `terraform plan --var-file=./my.tfvars`
 -- You will need to paste in your access key THEN your secret key

 6. Ensure there are no errors from the previous command.

 7. Run: `terraform apply --var-file=./my.tfvars`
 
    -- You will be asked to type in "yes" to continue. Once you enter "yes", terraform will spin up your environment in AWS.
    -- If you get an error with an "https" url, you may need to subscribe first to the AMI (Debain, Centos, etc), before we can spin them up.
       -- Follow the URL and Subscribe to the AMI
       -- Run apply again, and repeat the subscription process as needed

 8. SSH to your EC2 box using it's public IP

    `ssh -i mykey.pem user@publicIP`


